FROM botpress/server:ce-nightly-2019-01-08
ADD . /botpress
WORKDIR /botpress
CMD ["./bp"]
